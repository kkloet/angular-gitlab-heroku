import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { JwtToken } from './JwtToken';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Config } from '../../config';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) {}

  register(username: string, password: string) {
    return this.http.post(Config.API_URL + 'register', { username, password });
  }

  login(username: string, password: string) {
    const http = this.http;
    return new Promise((resolve, reject) => {
      http.post<JwtToken>(Config.API_URL + 'login', { username, password }).subscribe(data => {
        const token = data.token;
        const expiresIn = data.expiresIn;
        const currentDate = new Date();
        const expiryDate = new Date(currentDate.getTime() + expiresIn * 60000);

        localStorage.setItem('token', token);
        localStorage.setItem('expires_at', JSON.stringify(expiryDate.valueOf()));
        resolve();
      });
    });
  }

  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');
  }

  isLoggedIn() {
    const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);
  }
  getUserId() {
    return this.http.get(Config.API_URL + 'getuserid');
  }
}
