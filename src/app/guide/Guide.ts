import { RatingObject } from './rating/Rating';
import { Comment } from './comments/Comment';

export class Guide {
  id: string;
  author: string;
  authorId: string;
  title: string;
  gameTitle: string;
  releaseDate: Date;
  lastUpdated: Date;
  content: string;
  upvotes: number;
  downvotes: number;
  ratings: RatingObject[];
  comments: Comment[];

  constructor(
    id: string,
    author: string,
    authorId: string,
    title: string,
    gameTitle: string,
    releaseDate: Date,
    lastUpdated: Date,
    content: string,
    upvotes: number,
    downvotes: number,
    ratings: RatingObject[],
    comments: any[]
  ) {
    this.id = id;
    this.author = author;
    this.authorId = authorId;
    this.title = title;
    this.gameTitle = gameTitle;
    this.releaseDate = releaseDate;
    this.lastUpdated = lastUpdated;
    this.content = content;
    this.upvotes = upvotes;
    this.downvotes = downvotes;
    this.ratings = ratings;
    this.comments = comments;
  }
}
