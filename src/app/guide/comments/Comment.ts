export class Comment {
  id: string;
  author: string;
  content: string;
  guide: string;
  creationDate: Date;
  lastUpdated: Date;
  children: Comment[];

  constructor(
    id: string,
    author: string,
    content: string,
    guide: string,
    creationDate: Date,
    lastUpdated: Date,
    children: Comment[]
  ) {
    this.id = id;
    this.author = author;
    this.content = content;
    this.guide = guide;
    this.creationDate = creationDate;
    this.lastUpdated = lastUpdated;
    this.children = children;
  }
}
