import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../../../config';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();
  constructor(private http: HttpClient) {}
  // Service message commands
  emitChange(change: any) {
    this.emitChangeSource.next(change);
  }

  createComment(content: string, guideId: string) {
    return this.http
      .post(Config.API_URL + 'guide/' + guideId + '/comment', { content })
      .toPromise()
      .then(() => {
        this.emitChange('updated');
      });
  }

  createSubComment(content: string, guideId: string, parentCommentId: string) {
    return this.http
      .post(Config.API_URL + 'guide/' + guideId + '/comment/' + parentCommentId, { content })
      .toPromise()
      .then(() => {
        this.emitChange('updated');
      });
  }

  updateComment(id: string, content: string) {
    return this.http
      .put(Config.API_URL + 'comment/' + id, { content })
      .toPromise()
      .then(() => {
        this.emitChange('updated');
      });
  }

  deleteComment(id: string) {
    return this.http
      .delete(Config.API_URL + 'comment/' + id)
      .toPromise()
      .then(() => {
        this.emitChange('updated');
      });
  }
}
