import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentsComponent } from './comments.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { JwtModule } from '@auth0/angular-jwt';
import { tokenGetter } from '../../app.module';
import { Component, Input } from '@angular/core';

describe('CommentComponent', () => {
  let component: CommentsComponent;
  let fixture: ComponentFixture<CommentsComponent>;

  @Component({ selector: 'app-update-comment', template: '' })
  class UpdateCommentStubComponent {
    @Input() commentId: string;
    @Input() content: string;
  }

  @Component({ selector: 'app-create-comment', template: '' })
  class CreateCommentStubComponent {
    @Input() parentCommentId: string;
    @Input() guideId: string;
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        HttpClientTestingModule,
        JwtModule.forRoot({
          config: {
            tokenGetter
          }
        })
      ],
      declarations: [CommentsComponent, UpdateCommentStubComponent, CreateCommentStubComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
