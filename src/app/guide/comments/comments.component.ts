import { Component, Input, OnInit } from '@angular/core';
import { Comment } from './Comment';
import { AuthService } from '../../auth/auth.service';
import { CommentService } from './comment.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss']
})
export class CommentsComponent implements OnInit {
  @Input() comments: Comment[] = new Array();
  userId: string;
  updateActive = {};
  constructor(private authService: AuthService, private commentService: CommentService) {}

  ngOnInit() {
    this.authService.getUserId().subscribe(res => {
      const parsedResponse = JSON.parse(JSON.stringify(res));
      this.userId = parsedResponse.user_id;
    });
  }

  updateCommentToggle(id: string) {
    if (!Object.keys(this.updateActive).length) {
      // object is empty
      this.updateActive[id] = true;
    } else {
      for (const key in this.updateActive) {
        if (key === id) {
          // update existing
          this.updateActive[key] = !this.updateActive[key];
        } else {
          // add new key
          this.updateActive[id] = true;
        }
      }
    }
  }

  deleteComment(id: string) {
    this.commentService.deleteComment(id);
  }
}
