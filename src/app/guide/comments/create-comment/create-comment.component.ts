import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommentService } from '../comment.service';

@Component({
  selector: 'app-create-comment',
  templateUrl: './create-comment.component.html',
  styleUrls: ['./create-comment.component.scss']
})
export class CreateCommentComponent implements OnInit {
  @Input() guideId: string;
  @Input() parentCommentId: string;
  createCommentForm: FormGroup;
  throwError = false;
  visible = false;

  constructor(private formBuilder: FormBuilder, private commentService: CommentService) {}

  ngOnInit() {
    this.createCommentForm = this.formBuilder.group({
      content: ['', Validators.required]
    });
  }

  createComment() {
    const content = this.createCommentForm.value.content;
    if (this.parentCommentId !== undefined) {
      this.commentService.createSubComment(content, this.guideId, this.parentCommentId).then(res => {
        this.toggleVisibility();
        this.createCommentForm.reset();
      });
    } else {
      this.commentService.createComment(content, this.guideId).then(res => {
        this.toggleVisibility();
        this.createCommentForm.reset();
      });
    }
  }

  toggleVisibility() {
    this.visible = !this.visible;
  }
}
