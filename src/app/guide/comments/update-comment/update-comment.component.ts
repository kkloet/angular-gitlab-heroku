import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CommentService } from '../comment.service';

@Component({
  selector: 'app-update-comment',
  templateUrl: './update-comment.component.html',
  styleUrls: ['./update-comment.component.scss']
})
export class UpdateCommentComponent implements OnInit {
  @Input() commentId: string;
  @Input() content: string;
  @Output() updated = new EventEmitter<string>();
  updateCommentForm: FormGroup;
  throwError = false;
  constructor(private formBuilder: FormBuilder, private commentService: CommentService) {}

  ngOnInit() {
    this.updateCommentForm = this.formBuilder.group({
      content: [this.content, Validators.required]
    });
  }

  updateComment() {
    const content = this.updateCommentForm.value.content;
    this.commentService.updateComment(this.commentId, content).then(() => {
      this.updated.next('updated');
    });
  }
}
