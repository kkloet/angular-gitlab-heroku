import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OverviewComponent } from './overview/overview.component';
import { ReadGuideComponent } from './read-guide/read-guide.component';
import { NewGuideComponent } from './new-guide/new-guide.component';
import { UpdateGuideComponent } from './update-guide/update-guide.component';
import { AuthGuardService as AuthGuard } from '../auth/auth-guard.service';

const routes: Routes = [
  {
    path: 'guides',
    component: OverviewComponent,
    canActivate: [AuthGuard],
    children: [{ path: 'new', component: NewGuideComponent, canActivate: [AuthGuard] }]
  },
  {
    path: 'guides/:id',
    component: ReadGuideComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'guides/update/:id',
    component: UpdateGuideComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuideRoutingModule {}
