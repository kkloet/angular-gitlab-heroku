import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuideRoutingModule } from './guide-routing.module';
import { UpdateGuideComponent } from './update-guide/update-guide.component';
import { OverviewComponent } from './overview/overview.component';
import { ReadGuideComponent } from './read-guide/read-guide.component';
import { RatingComponent } from './rating/rating.component';
import { CommentsComponent } from './comments/comments.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateCommentComponent } from './comments/create-comment/create-comment.component';
import { UpdateCommentComponent } from './comments/update-comment/update-comment.component';

@NgModule({
  declarations: [
    UpdateGuideComponent,
    OverviewComponent,
    ReadGuideComponent,
    RatingComponent,
    CommentsComponent,
    CreateCommentComponent,
    UpdateCommentComponent
  ],
  imports: [CommonModule, ReactiveFormsModule, GuideRoutingModule]
})
export class GuideModule {}
