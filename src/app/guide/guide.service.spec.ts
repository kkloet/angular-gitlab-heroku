import { TestBed } from '@angular/core/testing';

import { GuideService } from './guide.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';

describe('GuideService', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, ReactiveFormsModule]
    })
  );

  it('should be created', () => {
    const service: GuideService = TestBed.get(GuideService);
    expect(service).toBeTruthy();
  });
});
