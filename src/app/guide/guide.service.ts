import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Config } from '../../config';
import { map } from 'rxjs/operators';
import { Guide } from './Guide';
import { Subject } from 'rxjs';
import { RatingObject } from './rating/Rating';

@Injectable({
  providedIn: 'root'
})
export class GuideService {
  // Observable string sources
  private emitChangeSource = new Subject<any>();
  // Observable string streams
  changeEmitted$ = this.emitChangeSource.asObservable();

  constructor(private http: HttpClient) {}

  // Service message commands
  emitChange(change: any) {
    this.emitChangeSource.next(change);
  }

  allGuides(sortBy: string) {
    let params = new HttpParams();
    if (sortBy) {
      params = params.append('sortBy', sortBy);
    }
    return this.http.get(Config.API_URL + 'guide', { params }).pipe(
      map((res: string) => {
        const parsedResponse = JSON.parse(JSON.stringify(res));
        const guides: Guide[] = new Array();
        for (const entry of parsedResponse.guides) {
          const releaseDate = new Date(entry.releaseDate);
          let lastUpdated = null;
          if (entry.lastUpdated) {
            lastUpdated = new Date(lastUpdated);
          }
          const ratings: RatingObject[] = new Array();
          for (const ratingEntry of entry.ratings) {
            const rating = new RatingObject(ratingEntry.ratingValue, ratingEntry.user);
            ratings.push(rating);
          }
          const guide = new Guide(
            entry._id,
            entry.author.username,
            entry.author._id,
            entry.title,
            entry.gameTitle,
            releaseDate,
            lastUpdated,
            entry.content,
            entry.upvotes,
            entry.downvotes,
            ratings,
            entry.comments
          );
          guides.push(guide);
        }
        return guides;
      })
    );
  }

  readGuide(id: string) {
    return this.http.get(Config.API_URL + 'guide/' + id).pipe(
      map((res: string) => {
        const parsedResponse = JSON.parse(JSON.stringify(res));
        const releaseDate = new Date(parsedResponse.guide.releaseDate);
        let lastUpdated = null;
        if (parsedResponse.guide.lastUpdated) {
          lastUpdated = new Date(lastUpdated);
        }
        const ratings: RatingObject[] = new Array();
        for (const entry of parsedResponse.guide.ratings) {
          const rating = new RatingObject(entry.ratingValue, entry.user);
          ratings.push(rating);
        }
        const guide = new Guide(
          parsedResponse.guide._id,
          parsedResponse.guide.author.username,
          parsedResponse.guide.author._id,
          parsedResponse.guide.title,
          parsedResponse.guide.gameTitle,
          releaseDate,
          lastUpdated,
          parsedResponse.guide.content,
          parsedResponse.guide.upvotes,
          parsedResponse.guide.downvotes,
          ratings,
          parsedResponse.guide.comments
        );
        return guide;
      })
    );
  }

  createGuide(guide: Guide) {
    const reqBody = {
      title: guide.title,
      gameTitle: guide.gameTitle,
      content: guide.content
    };
    return this.http
      .post(Config.API_URL + 'guide', reqBody)
      .toPromise()
      .then(res => {
        console.log('EMITTING EVENT');
        this.emitChange('updated');
      });
  }

  update(guide: Guide) {
    const reqBody = {
      title: guide.title,
      gameTitle: guide.gameTitle,
      content: guide.content
    };
    return this.http.put(Config.API_URL + 'guide/' + guide.id, reqBody);
  }

  remove(guideId: string) {
    return this.http.delete(Config.API_URL + 'guide/' + guideId);
  }

  vote(id: string, value: number) {
    return this.http
      .post(Config.API_URL + '/guide/' + id + '/vote', { rating: value })
      .toPromise()
      .then(() => {
        this.emitChange('updated');
      });
  }
}
