import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Guide } from '../Guide';
import { GuideService } from '../guide.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-new-guide',
  templateUrl: './new-guide.component.html',
  styleUrls: ['./new-guide.component.scss']
})
export class NewGuideComponent implements OnInit {
  createGuideForm: FormGroup;
  throwError = false;
  constructor(
    private formBuilder: FormBuilder,
    private guideService: GuideService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.createGuideForm = this.formBuilder.group({
      title: ['', Validators.required],
      gameTitle: [''],
      content: ['', Validators.required]
    });
  }

  createGuide() {
    if (this.createGuideForm.invalid) {
      this.throwError = true;
      console.log('invalid');
      return;
    }
    const title = this.createGuideForm.value.title;
    const gameTitle = this.createGuideForm.value.gameTitle;
    const content = this.createGuideForm.value.content;
    const guide = new Guide('', '', '', title, gameTitle, null, null, content, 0, 0, null, null);
    this.guideService.createGuide(guide);
    return this.router.navigate(['..'], { relativeTo: this.route });
  }
}
