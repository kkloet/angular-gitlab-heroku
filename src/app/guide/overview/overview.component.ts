import { Component, OnInit } from '@angular/core';
import { GuideService } from '../guide.service';
import { Guide } from '../Guide';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  guides: Guide[];
  sortBy = '';
  constructor(private guideService: GuideService) {}

  ngOnInit() {
    this.updateList();
    this.guideService.changeEmitted$.subscribe(() => {
      this.updateList();
    });
  }

  setSortBy(sortBy: string) {
    this.sortBy = sortBy;
    this.updateList();
  }

  updateList() {
    return new Promise((resolve, reject) => {
      this.guideService.allGuides(this.sortBy).subscribe(guides => {
        this.guides = guides;
        resolve();
      });
    });
  }
}
