export class RatingObject {
  ratingValue: number;
  userId: string;

  constructor(ratingValue: number, userId: string) {
    this.ratingValue = ratingValue;
    this.userId = userId;
  }
}
