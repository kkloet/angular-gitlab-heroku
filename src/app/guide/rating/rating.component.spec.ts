import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingComponent } from './rating.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { JwtModule } from '@auth0/angular-jwt';
import { tokenGetter } from '../../app.module';
import { RouterTestingModule } from '@angular/router/testing';
import { RatingObject } from './Rating';
import { GuideService } from '../guide.service';
import { AuthService } from '../../auth/auth.service';
import { of } from 'rxjs';

describe('RatingComponent', () => {
  let component: RatingComponent;
  let fixture: ComponentFixture<RatingComponent>;
  let guideServiceSpy: { vote: jasmine.Spy };
  let authServiceSpy: { getUserId: jasmine.Spy };

  beforeEach(async(() => {
    guideServiceSpy = jasmine.createSpyObj('guideService', ['vote']);
    authServiceSpy = jasmine.createSpyObj('authService', ['getUserId']);
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        JwtModule.forRoot({
          config: {
            tokenGetter
          }
        })
      ],
      declarations: [RatingComponent],
      providers: [
        { provide: GuideService, useValue: guideServiceSpy },
        { provide: AuthService, useValue: authServiceSpy }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    authServiceSpy.getUserId.and.returnValue(of({ user_id: '1' }));
    guideServiceSpy.vote.and.returnValue(of({ message: 'added vote' }).toPromise());
    fixture = TestBed.createComponent(RatingComponent);
    component = fixture.componentInstance;
    const ratings: RatingObject[] = new Array();
    component.ratings = ratings;
    component.guideId = '1';
    fixture.detectChanges();
  });

  it('should create', done => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component).toBeTruthy();
      done();
    });
  });

  it('should add a vote', done => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component).toBeTruthy();
      component.vote(1).then(() => {
        expect(component.ratings.length).toBe(1);
        expect(component.ratings[0].ratingValue).toBe(1);
        expect(component.ratings[0].userId).toBe('1');
        expect(component.upvotes).toBe(1);
        expect(component.downvotes).toBe(0);
        done();
      });
    });
  });
});
