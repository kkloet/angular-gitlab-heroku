import { Component, Input, OnInit } from '@angular/core';
import { RatingObject } from './Rating';
import { GuideService } from '../guide.service';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {
  @Input() ratings: RatingObject[];
  @Input() guideId: string;
  upvotes = 0;
  downvotes = 0;
  userHasUpvote = false;
  userHasDownvote = false;

  constructor(private guideService: GuideService, private authService: AuthService) {}

  ngOnInit() {
    this.updateRatings();
    this.updateUserRating();
  }

  getRating() {
    return this.upvotes - this.downvotes;
  }

  vote(val: number) {
    if ((this.userHasUpvote && val === 1) || (this.userHasDownvote && val === -1)) {
      val = 0;
    }
    return this.guideService.vote(this.guideId, val).then(() => {
      this.authService
        .getUserId()
        .toPromise()
        .then(res => {
          const id = JSON.parse(JSON.stringify(res));
          const userRatings = this.ratings.filter(a => a.userId === id.user_id);
          if (userRatings.length > 0) {
            userRatings.forEach(x => (x.ratingValue = val));
          } else {
            const rating = new RatingObject(val, id.user_id);
            this.ratings.push(rating);
          }
          this.updateRatings();
          this.updateUserRating();
        });
    });
  }

  private updateRatings() {
    this.upvotes = 0;
    this.downvotes = 0;
    this.ratings.forEach(rating => {
      if (rating.ratingValue > 0) {
        this.upvotes++;
      } else if (rating.ratingValue < 0) {
        this.downvotes++;
      }
    });
  }

  private updateUserRating() {
    this.authService
      .getUserId()
      .toPromise()
      .then(res => {
        this.userHasUpvote = false;
        this.userHasDownvote = false;
        const id = JSON.parse(JSON.stringify(res));
        const userVotes = this.ratings.filter(e => e.userId === id.user_id);
        if (userVotes.some(v => v.ratingValue > 0)) {
          this.userHasUpvote = true;
        } else if (userVotes.some(v => v.ratingValue < 0)) {
          this.userHasDownvote = true;
        }
      });
  }
}
