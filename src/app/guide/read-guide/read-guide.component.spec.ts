import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Location } from '@angular/common';
import { ReadGuideComponent } from './read-guide.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { JwtModule } from '@auth0/angular-jwt';
import { tokenGetter } from '../../app.module';
import { RatingComponent } from '../rating/rating.component';
import { Component, Input } from '@angular/core';
import { Guide } from '../Guide';
import { ActivatedRoute } from '@angular/router';
import { GuideService } from '../guide.service';
import { AuthService } from '../../auth/auth.service';
import { of } from 'rxjs';
import { RatingObject } from '../rating/Rating';

const dummyGuide = new Guide(
  '1',
  'testUser',
  '1',
  'test title',
  'my game',
  new Date(),
  null,
  'test guide',
  0,
  0,
  [new RatingObject(1, '1')],
  null
);

describe('ReadGuideComponent', () => {
  let component: ReadGuideComponent;
  let fixture: ComponentFixture<ReadGuideComponent>;

  let guideServiceSpy: { readGuide: jasmine.Spy; remove: jasmine.Spy };
  let authServiceSpy: { getUserId: jasmine.Spy };

  @Component({ selector: 'app-guides', template: '' })
  class GuidesStubComponent {}

  @Component({ selector: 'app-rating', template: '' })
  class RatingStubComponent {
    @Input() guideId: string;
    @Input() ratings: RatingComponent[];
  }

  @Component({ selector: 'app-create-comment', template: '' })
  class CreateCommentStubComponent {
    @Input() guideId: string;
  }

  @Component({ selector: 'app-comments', template: '' })
  class CommentsStubComponent {
    @Input() comments: Comment[];
  }

  @Component({ selector: 'app-update-comment', template: '' })
  class UpdateCommentStubComponent {
    @Input() commentId: string;
    @Input() content: string;
  }

  beforeEach(async(() => {
    guideServiceSpy = jasmine.createSpyObj('guideService', ['readGuide', 'remove']);
    authServiceSpy = jasmine.createSpyObj('authService', ['getUserId']);
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          { path: '', component: ReadGuideComponent },
          { path: 'guides', component: GuidesStubComponent }
        ]),
        HttpClientTestingModule,
        ReactiveFormsModule,
        JwtModule.forRoot({
          config: {
            tokenGetter
          }
        })
      ],
      declarations: [
        ReadGuideComponent,
        RatingStubComponent,
        CreateCommentStubComponent,
        CommentsStubComponent,
        UpdateCommentStubComponent,
        GuidesStubComponent
      ],
      providers: [
        { provide: GuideService, useValue: guideServiceSpy },
        { provide: AuthService, useValue: authServiceSpy },
        { provide: ActivatedRoute, useValue: { snapshot: { params: { id: '1' } } } }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    authServiceSpy.getUserId.and.returnValue(of({ user_id: '1' }));
    guideServiceSpy.readGuide.withArgs(dummyGuide.id).and.returnValue(of(dummyGuide));
    guideServiceSpy.remove.withArgs(dummyGuide.id).and.returnValue(of({ message: 'removed' }));

    fixture = TestBed.createComponent(ReadGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    fixture.destroy();
  });

  it('should create', async(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component).toBeTruthy();
    });
  }));

  it('should load the guide', async(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component).toBeTruthy();
      component.getGuide().then(() => {
        expect(component.guide.id).toBe(dummyGuide.id);
        expect(component.guide.content).toBe(dummyGuide.content);
        expect(component.guide.gameTitle).toBe(dummyGuide.gameTitle);
        expect(component.guide.authorId).toBe(dummyGuide.authorId);
        expect(component.guide.ratings.length).toBe(1);
        expect(component.guide.ratings[0].userId).toBe('1');
      });
    });
  }));

  it('should navigate back to the guides component', done => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component).toBeTruthy();
      fixture.ngZone.run(() => {
        component.goBack().then(() => {
          const location = TestBed.get(Location);
          expect(location.path()).toBe('/guides');
          done();
        });
      });
    });
  });

  it('should navigate to the guides component after removing the guide', done => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component).toBeTruthy();
      fixture.ngZone.run(() => {
        component.remove().then(() => {
          const location = TestBed.get(Location);
          expect(location.path()).toBe('/guides');
          done();
        });
      });
    });
  });
});
