import { Component, OnInit } from '@angular/core';
import { GuideService } from '../guide.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Guide } from '../Guide';
import { AuthService } from '../../auth/auth.service';
import { CommentService } from '../comments/comment.service';

@Component({
  selector: 'app-read-guide',
  templateUrl: './read-guide.component.html',
  styleUrls: ['./read-guide.component.scss']
})
export class ReadGuideComponent implements OnInit {
  guide: Guide;
  userId: string;
  constructor(
    private guideService: GuideService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthService,
    private commentService: CommentService
  ) {}

  ngOnInit() {
    this.getGuide();
    this.authService.getUserId().subscribe(res => {
      const parsedResponse = JSON.parse(JSON.stringify(res));
      this.userId = parsedResponse.user_id;
    });
    this.commentService.changeEmitted$.subscribe(() => {
      this.getGuide();
    });
  }
  goBack() {
    return this.router.navigateByUrl('/guides');
  }
  remove() {
    return this.guideService
      .remove(this.guide.id)
      .toPromise()
      .then(() => {
        this.router.navigateByUrl('/guides');
      });
  }
  getGuide() {
    const id = this.route.snapshot.params['id'];
    return this.guideService
      .readGuide(id)
      .toPromise()
      .then(guide => {
        this.guide = guide;
      });
  }
}
