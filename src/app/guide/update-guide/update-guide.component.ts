import { Component, OnInit } from '@angular/core';
import { Guide } from '../Guide';
import { GuideService } from '../guide.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-update-guide',
  templateUrl: './update-guide.component.html',
  styleUrls: ['./update-guide.component.scss']
})
export class UpdateGuideComponent implements OnInit {
  guide: Guide;
  updateGuideForm: FormGroup;
  throwError = false;

  constructor(
    private formBuilder: FormBuilder,
    private guideService: GuideService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.guideService
      .readGuide(id)
      .toPromise()
      .then(guide => {
        this.guide = guide;
      });
    this.updateGuideForm = this.formBuilder.group({
      title: ['', Validators.required],
      gameTitle: [''],
      content: ['', Validators.required]
    });
  }

  submit() {
    this.guideService
      .update(this.guide)
      .toPromise()
      .then(() => {
        this.router.navigateByUrl('/guides/' + this.guide.id);
      });
  }
}
