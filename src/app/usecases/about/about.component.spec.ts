import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutComponent } from './about.component';
import { Component, Input } from '@angular/core';
import { UseCase } from '../usecase.model';

describe('AboutComponent', () => {
  let component: AboutComponent;
  let fixture: ComponentFixture<AboutComponent>;

  @Component({ selector: 'app-usecase', template: '' })
  class UsecaseStubComponent {
    @Input() useCase: UseCase;
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AboutComponent, UsecaseStubComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
