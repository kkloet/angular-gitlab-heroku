import { Component, OnInit } from '@angular/core';
import { UsecaseService } from '../usecase.service';
import { UseCase } from '../usecase.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
  useCases: UseCase[];
  constructor(private usecaseService: UsecaseService) {}

  ngOnInit() {
    this.getUsecases();
  }
  getUsecases(): void {
    this.usecaseService.getUsecases().subscribe(usecases => (this.useCases = usecases));
  }
}
