import { UseCase } from './usecase.model';

export const USECASES: UseCase[] = [
  {
    id: 'UC-01',
    name: 'Gebruiker registreren',
    description: 'De gebruiker moet zich met een gebruikersnaam en wachtwoord kunnen registreren',
    scenario: [
      'Gebruiker vult een gebruikersnaam in.',
      'Gebruiker vult een wachtwoord in.',
      'Gebruiker klikt op registreren knop.',
      'Server controleert input.',
      'Server maakt een account aan.'
    ],
    actor: 'Gebruiker',
    preCondition: 'Gebruiker heeft geen account en is op de registratiepagina.',
    result: 'De gebruiker wordt doorgestuurd naar de login pagina waar hij/zij kan inloggen.'
  },
  {
    id: 'UC-02',
    name: 'Gebruiker inloggen',
    description: 'De gebruiker moet zich met een gebruikersnaam en wachtwoord kunnen inloggen',
    scenario: [
      'Gebruiker vult een gebruikersnaam in.',
      'Gebruiker vult een wachtwoord in.',
      'Gebruiker klikt op login knop.',
      'Server controleert input.',
      'Server authenticeert gebruiker.'
    ],
    actor: 'Gebruiker',
    preCondition: 'Gebruiker heeft een account.',
    result: 'De gebruiker wordt doorgestuurd naar de home pagina.'
  },
  {
    id: 'UC-03',
    name: 'Guide aanmaken',
    description: 'De gebruiker moet een nieuwe guide kunnen aanmaken',
    scenario: [
      'Gebruiker vult een titel in.',
      'optioneel: Gebruiker vult een game titel in.',
      'Gebruiker schrijft de guide.',
      'Gebruiker klikt op guide posten knop',
      'Server slaat de guide op'
    ],
    actor: 'Gebruiker',
    preCondition: 'Gebruiker heeft op nieuwe guide knop gedrukt.',
    result: 'De gebruiker wordt doorgestuurd naar zijn nieuw gemaakte guide.'
  },
  {
    id: 'UC-04',
    name: 'Guide aanpassen',
    description: 'De gebruiker moet zijn eigen guides kunnen aanpassen',
    scenario: [
      'Gebruiker wijzigt velden waar nodig',
      'Gebruiker klikt op update knop',
      'Server slaat updates op'
    ],
    actor: 'Auteur',
    preCondition: 'Gebruiker heeft op edit knop van een eigen guide geklikt.',
    result: 'De gebruiker wordt doorgestuurd naar zijn guide waar de wijzigingen zichtbaar zijn.'
  },
  {
    id: 'UC-05',
    name: 'Guide verwijderen',
    description: 'De gebruiker moet zijn eigen guides kunnen verwijderen',
    scenario: ['Gebruiker klikt op verwijderen knop.', 'Server verwijderd de guide'],
    actor: 'Auteur',
    preCondition: 'Gebruiker heeft guides.',
    result: 'De guide is verwijderd.'
  },
  {
    id: 'UC-06',
    name: 'Guide lezen',
    description: 'De gebruiker moet een guide kunnen openen',
    scenario: ['Gebruiker klikt op de titel van een guide.', 'Server laadt guide in.'],
    actor: 'Gebruiker',
    preCondition: 'Gebruiker is ingelogd',
    result: 'De gebruiker krijgt de guide te zien.'
  },
  {
    id: 'UC-07',
    name: 'Guide beoordelen',
    description: 'De gebruiker moet een guide kunnen up of downvoten',
    scenario: ['Gebruiker klikt op upvote of downvote knop', 'Server past beoordeling toe'],
    actor: 'Gebruiker',
    preCondition: 'Gebruiker heeft een guide geopend.',
    result: 'De guide beoordeling wordt aangepast afhankelijk van een up of downvote..'
  },
  {
    id: 'UC-08',
    name: 'Comment plaatsen',
    description: 'De gebruiker moet een comments kunnen plaatsen op een guide',
    scenario: [
      'Gebruiker klikt add comments',
      'Gebruiker schrijft een comments.',
      'Server voegt comments toe aan guide'
    ],
    actor: 'Gebruiker',
    preCondition: 'Gebruiker heeft een guide geopend.',
    result: 'Een comments is aan de guide toegevoegd.'
  },
  {
    id: 'UC-09',
    name: 'Comment op comments plaatsen',
    description: 'De gebruiker moet een comments kunnen plaatsen op een andere comments',
    scenario: [
      'Gebruiker klikt add comments onder een andere comments',
      'Gebruiker schrijft een comments.',
      'Server voegt comments toe aan comments'
    ],
    actor: 'Gebruiker',
    preCondition: 'Gebruiker heeft een guide geopend, er is een comments',
    result: 'Een comments is aan de comments toegevoegd.'
  },
  {
    id: 'UC-10',
    name: 'Comment updaten',
    description: 'De gebruiker moet een comments kunnen updaten',
    scenario: [
      'Gebruiker klikt edit comments.',
      'Gebruiker herschrijft een comments.',
      'Server updatet comments'
    ],
    actor: 'Comment auteur',
    preCondition: 'Gebruiker heeft een comments.',
    result: 'comments is geupdatet.'
  },
  {
    id: 'UC-11',
    name: 'Comment verwijderen',
    description: 'De gebruiker moet een comments kunnen verwijderen',
    scenario: ['Gebruiker klikt delete comments', 'Server verwijdert comments'],
    actor: 'Comment auteur',
    preCondition: 'Gebruiker heeft een comments.',
    result: 'Comment is verwijderd.'
  },
  {
    id: 'UC-12',
    name: 'Guides sorteren',
    description: 'De gebruiker moet guides kunnen sorteren',
    scenario: [
      'Gebruiker selecteerd waar hij op wilt sorteren. [release datum of populariteit]',
      'Server sorteert de guides'
    ],
    actor: 'Gebruiker',
    preCondition: 'Gebruiker is op homepagina of heeft een zoekquery uitgevoerd',
    result: 'lijst met guides is gesorteerd.'
  }
];
