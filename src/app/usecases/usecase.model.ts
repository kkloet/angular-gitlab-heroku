export class UseCase {
  id: string
  name: string
  scenario: string[]
  actor: string
  preCondition: string
  description: string
  result: string
}
