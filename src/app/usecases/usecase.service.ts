import { Injectable } from '@angular/core';
import { UseCase } from './usecase.model';
import { USECASES } from './usecase.mock';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UsecaseService {
  getUsecases(): Observable<UseCase[]> {
    return of(USECASES);
  }
  constructor() {}
}
